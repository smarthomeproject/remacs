<?php

include "../utils/db-connect.php";

class User {
    
    private $statement;
    private $conn;
    
    //constructor
    function __construct() {
        
        //file path
        $filename = "statements.json";
        
        //read statements file
        $handle = fopen($filename, "r");
        $content = fread($handle, filesize($filename));
        
        //parse json
        $this->statement = json_decode($content, true);
        
        //create the database connection
        $dbConnect = new DBConnect();
        $this->conn = $dbConnect->getInstance();        
    }
    
    //desctructor
    function __destruct() {
        
    }
    
    public function getAllUsers() {
        
        $query = $this->statement["0"];
        $query = str_replace("%TABLE%", "user", $query);
        $result = $this->conn->query($query);
        
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    
    public function getSearch($column, $value) {
        //select statement
        $query = $this->statement["9"];
        //replace as required
        $query = str_replace("%TABLE%", "user", $query);
        $query = str_replace("%COLUMN%", $column, $query);
        $query = str_replace("%PARAMETER%", $value, $query);
        //run query
        $result = $this->conn->query($query);
        //return result
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getSingleUser($column, $value) {
        //select statement
        $query = $this->statement["1"];
        //replace as required
        $query = str_replace("%TABLE%", "user", $query);
        $query = str_replace("%COLUMN%", $column, $query);
        $query = str_replace("%PARAMETER%", $value, $query);
        //run query
        $result = $this->conn->query($query);
        //return result
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    
    public function insertUser($valuesArray) {
        //select statement
        $query = $this->statement["2"];
        //replace as required
        $temp = "";
        $index = 0;
        foreach($valuesArray as $key => $value) {
            
            $index++;
            
            if($index < count($valuesArray)) {
                $temp .= "" . $key . "='" . $value . "', ";
            }
            else {
                $temp .= "" . $key . "='" . $valuesArray[$key] . "'";
            }

        }
        
        $query = str_replace("%TABLE%", "user", $query);
        $query = str_replace("%VALUES%", $temp, $query);
        
        //run query
        $result = $this->conn->query($query);
        
        return $result;
    }
    
    public function updateUser($valuesArray,$column,$conditon) {
        //select statement
        $query = $this->statement["3"];
        //replace as required
        $temp = "";
        $index = 0;
        foreach($valuesArray as $key => $value) {
            
            $index++;
            
            if($index < count($valuesArray)) {
                $temp .= "`" . $key . "`='" . $value . "', ";
            }
            else {
                $temp .= "`" . $key . "`='" . $valuesArray[$key] . "'";
            }
        }
        
        $query = str_replace("%TABLE%", "user", $query);
        $query = str_replace("%VALUES%", $temp, $query);
        $query = str_replace("%COLUMN%",$column,$query);
        $query = str_replace("%PARAMETER%",$conditon,$query);
        
        //run query
        $result = $this->conn->query($query);

        return $result;
    }
    
    public function deleteUser($column, $value) {
        //select statement
        $query = $this->statement["4"];
        //replace as required
        $query = str_replace("%TABLE%", "user", $query);
        $query = str_replace("%COLUMN%", $column, $query);
        $query = str_replace("%VALUE%", $value, $query);
        //run query
        $result = $this->conn->query($query);

        return $result;
    }
}

/* section */

$user = new User();

if($_SERVER['REQUEST_METHOD'] == "GET") {

    if(isset($_GET['ready'])){
            $selectAll=$user->getAllUsers();
            echo json_encode($selectAll);
    }

    if(isset($_GET['search'])) {
            $search=$user->getSearch("username", "'%".$_GET['search']."%'");
            echo json_encode($search);
    }

    if(isset($_GET['username'])) {
            $singleUser=$user->getSingleUser("username", "'".$_GET['username']."'");
            echo json_encode($singleUser);
            //var_dump($singleUser);
    }
    if(isset($_GET['email'])) {
            $singleUser=$user->getSingleUser("email", "'".$_GET['email']."'");
            
            //var_dump($singleUser);
            echo json_encode($singleUser);
    }

    if(isset($_GET['serial'])) {
            $singleUser=$user->getSingleUser("serial_no", "'".$_GET['serial']."'");
            
            //var_dump($singleUser);
            echo json_encode($singleUser);
    }

}

if($_SERVER['REQUEST_METHOD'] == "POST") {

    if((isset($_POST['username'])) && (isset($_POST['serial_no']))) {
            $values=($_POST);
            $username="'".$_POST['username']."'";

            $update = $user->updateUser($values,"username", $username);
            
            //echo "succ";
    }
}

if ($_SERVER["REQUEST_METHOD"] == "PUT") {
      # code...
      parse_str(file_get_contents("php://input"),$array);
      
      $insertUser = $user-> insertUser($array)->affected_rows();
      var_dump($insertUser);     


}

    //inserting users 
     //$insert1=$user->insertUser($member1);
     //var_dump($insert1);

    //selecting all users
    // $selectAll=$user->getAllUsers();
    // var_dump($selectAll);

    //select single user
    // $selectSingle=$user->getSingleUser("email","'rifhan@yahoo.com'");
    // var_dump($selectSingle);


    // //updating member2 with member1 details
    // $update=$user->updateUser($member1,"email","'rifhan@yahoo.com'");
    // var_dump($update);

    //deleting member 2
    // $delete=$user->deleteUser("email","'Akram@test.com'");  
    // var_dump($delete);

?>