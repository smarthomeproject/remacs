<?php

	include "../utils/db-connect.php";

	class Equipment{

		private $statement;
		private $conn;

		//constructor
		function __construct() {

			$filename = "statements.json";

			$handle = fopen($filename, "r");
			$contents = fread($handle, filesize($filename));

			$this->statement = json_decode($contents, true);

			$dbConnect = new DBConnect();
			$this->conn = $dbConnect->getInstance();
		}

		//destructor
		function __destruct() {

		}

		function getEquipment($column, $value, $column1, $value1, $column2, $value2) {

			$query = $this->statement["6"];

			$query = str_replace("%TABLE%", "equipment", $query);
			$query = str_replace("%COLUMN%", $column, $query);
			$query = str_replace("%PARAMETER%", $value, $query);
			$query = str_replace("%COLUMN1%", $column1, $query);
			$query = str_replace("%PARAMETER1%", $value1, $query);
			$query = str_replace("%COLUMN2%", $column2, $query);
			$query = str_replace("%PARAMETER2%", $value2, $query);

			$result = $this->conn->query($query);

			return $result->fetch_all(MYSQLI_ASSOC);
		}

		function insertEquipment($valuesArray) {

			$query = $this->statement["2"];

			$temp = "";
			$index = 0;

			foreach($valuesArray as $key => $value) {

				$index++;

				if($index < count($valuesArray)) {
					$temp .= "" . $key . "='" . $value . "', ";
				}
				else{
					$temp .= "" .$key ."='" . $valuesArray[$key] . "'";
				}
			}

			$query = str_replace("%TABLE%", "equipment", $query);
			$query = str_replace("%VALUES%", $temp, $query);

			$result = $this->conn->query($query);

			return $result;
		}

		function updateEquipment($valuesArray, $column, $condition, $column1, $condition1) {

			$query = $this->statement["7"];

			$temp = "";
			$index = 0;

			foreach ($valuesArray as $key => $value) {
				
				$index++;

				if($index < count($valuesArray)) {
					$temp .= "" . $key . "='" . $value . "', ";
				}
				else {
					$temp .= "" . $key . "='" . $valuesArray[$key] . "'";
				}
			}

			$query = str_replace("%TABLE%", "equipment", $query);
			$query = str_replace("%VALUES%", $temp, $query);
			$query = str_replace("%COLUMN%", $column, $query);
			$query = str_replace("%PARAMETER%", $condition, $query);
			$query = str_replace("%COLUMN1%", $column1, $query);
			$query = str_replace("%PARAMETER1%", $condition1, $query);
		
			$result = $this->conn->query($query);

			return $result;
		}

		function deleteEquipment($column, $value, $column1, $value1, $column2, $value2) {

			$query = $this->statement["8"];

			$query = str_replace("%TABLE%", "equipment", $query);
			$query = str_replace("%COLUMN%", $column, $query);
			$query = str_replace("%VALUE%", $value, $query);
			$query = str_replace("%COLUMN%", $column1, $query);
			$query = str_replace("%VALUE%", $value1, $query);
			$query = str_replace("%COLUMN%", $column2, $query);
			$query = str_replace("%VALUE%", $value2, $query);

			$result = $this->conn->query($query);

			return $result;
		}

	}

	//testing

$equipment = new Equipment();

if($_SERVER['REQUEST_METHOD'] == "POST") {
	
    // if(isset($_POST['room_id'])) {
    //         $value = array('room_name' => ($_POST['room_name']));
    //         	//var_dump($value);
    //         $updateRoom = $room->updateRoom($value, "room_id", "'".$_POST['room_id']."'");
    // }

    if(isset($_POST['room_id'])) {
		for($i=0 ; $i<count($_POST['equip']); $i++) {
			$value = array('equipment_name' => ($_POST['equip'][$i]));
			$updateEquip = $equipment->updateEquipment($value, "equipment_number", "'".($_POST['addr'][$i])."'", "room_id", "'".$_POST['room_id']."'");
		}
	}

	if(isset($_POST['rumID'])) {
		for($i=0 ; $i<count($_POST['eq_no']); $i++) {
			$value = array(
				'hub_serial_number' => $_POST['serial_no'],
				'equipment_number' => $_POST['eq_no'][$i],
				'room_id' => $_POST['rumID'],
				'equipment_name' => $_POST['eq_name'][$i]
			);
			var_dump($value);
			$insertEquip = $equipment->insertEquipment($value);
		}
	}

}

if($_SERVER['REQUEST_METHOD'] == "GET") {

	if(isset($_GET['rumID'])) {
		$msg="";
		for($i=0 ; $i<count($_GET['eq_no']); $i++) {
			//$value = array('equipment_number' => ($_GET['eq_no'][$i]));
			//var_dump($value);
			$selectEquip = $equipment->getEquipment("equipment_number", "'".$_GET['eq_no'][$i]."'", "hub_serial_number", "'".$_GET['serial_no']."'", "room_id", "'".$_GET['rumID']."'");

			if($selectEquip) 
				$msg= "exist";
		}

		if(strcmp($msg, "exist") == 0)
			echo "Equipment Number exists";
		else
			echo "true";			
	}

}

if ($_SERVER["REQUEST_METHOD"] == "PUT") {
		# code...
		parse_str(file_get_contents("php://input"),$array);

		
			foreach ($array as $key => $value) {				
				$insertEquipment =  $equipment -> insertEquipment($value);
			}

		

}

	//GET EQUIPMENT
	//var_dump($equipment->getEquipment("room_id", "'ss34'"));
	//echo "<br/>";
	//var_dump($equipment->getEquipment("equipment_address", "'s123'"));

	//INSERT EQUIPMENT
	// $equip = [
	//     "hub_serial_number" => "sim187",
	//     "equipment_number" => "a",
	//     "room_id" => "RM001",
	//     "equipment_name" => "bulb",
	//     "state" => "1"
	// ];
	// $equipment->insertEquipment($equip);

	//UPDATE EQUIPMENT
	//  $updateEq = [
	//  	"state" => "0",
	// ];
	// $equipment->updateEquipment($updateEq, "hub_serial_number", "'sim187'" , "equipment_number", "'a'" , "room_id", "'RM001'");

	//DELETE EQUIPMENT
	// $equipment->deleteEquipment("equipment_address", "'s123'");	

?>