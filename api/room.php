<?php
	include "../utils/db-connect.php";

	class Room {
		private $statement;
		private $conn;

		//constructor
		function __construct() {
			//file name
			$filename = "statements.json";

			//read the file
			$handle = fopen($filename, "r");
			$content = fread($handle, filesize($filename));

			//parse JSON
			$this->statement = json_decode($content,true);

			//DB connection
			$dbConnect = new DBConnect();
			$this->conn = $dbConnect->getInstance();
		}

		//desctructor
	    function __destruct() {
	        
	    }

	    //returns all room details
	    public function selectRoom($column,$parameter) {
	    	//gets the value of the 1st index of the JSON file
	    	$query = $this->statement["1"];

	    	//replaces values with the given values
	    	$query = str_replace("%TABLE%", "room", $query);
	    	$query = str_replace("%COLUMN%", $column, $query);
	    	$query = str_replace("%PARAMETER%", $parameter, $query);

	    	//query run
	    	$result = $this->conn->query($query);

	    	//returns results as an array
	    	return $result->fetch_all(MYSQLI_ASSOC);
	    }

	    //insert values
	    public function insertRoom($valuesArray) {
			//gets the value of the 2nd index of the JSON file
	    	$query = $this->statement["2"];

	    	//make the final query to run
	    	$temp = "";
	        $index = 0;
	        foreach($valuesArray as $key => $value) {
	            
	            $index++;
	            
	            if($index < count($valuesArray)) {
	                $temp .= "" . $key . "='" . $value . "', ";
	            }
	            else {
	                $temp .= "" . $key . "='" . $valuesArray[$key] . "'";
	            }
	        }

	    	//replaces values with the given values
	    	$query = str_replace("%TABLE%", "room", $query);
	    	$query = str_replace("%VALUES%", $temp, $query);

	    	//query run
	    	$result = $this->conn->query($query);

	    	//returns result
	    	return $result;
	    }

	    //upadate values
	    public function updateRoom($valuesArray,$column,$conditon) {
	    	//gets the value of the 3rd index of the JSON file
	    	$query = $this->statement["3"];

	    	//make the final query to run
	    	$temp = "";
	        $index = 0;
	        foreach($valuesArray as $key => $value) {
	            
	            $index++;
	            
	            if($index < count($valuesArray)) {
	                $temp .= "" . $key . "='" . $value . "', ";
	            }
	            else {
	                $temp .= "" . $key . "='" . $valuesArray[$key] . "'";
	            }
	        }

	    	//replaces values with the given values
	    	$query = str_replace("%TABLE%", "room", $query);
	    	$query = str_replace("%VALUES%", $temp, $query);
	    	$query = str_replace("%COLUMN%", $column, $query);
	    	$query = str_replace("%PARAMETER%", $conditon, $query);

	    	//query run
	    	$result = $this->conn->query($query);

	    	//returns result
	    	return $result;	
	    }

	    //delete values
	    public function deleteRoom($column, $value) {
	    	//gets the value of the 4th index of the JSON file
	    	$query = $this->statement["4"];

	    	//replaces values with the given values
	    	$query = str_replace("%TABLE%", "room", $query);
	    	$query = str_replace("%COLUMN%", $column, $query);
	    	$query = str_replace("%VALUE%", $value, $query);

	    	//query run
	    	$result = $this->conn->query($query);

	    	//returns result
	    	return $result;
	    }
	}

	/* This section only to be used for testing purposes */

$room = new Room();

$servername = "localhost";
$username = "root";
$password = "";
$database = "smart_home";

$conn = new mysqli($servername, $username, $password, $database);

//$dbConnect = new DBConnect();
//$this->conn = $dbConnect->getInstance();

if($_SERVER['REQUEST_METHOD'] == "GET") {

    if(isset($_GET['serialNo'])){

    	$query = "select * from room where serial_no='".$_GET['serialNo']."'";
			$result = $conn->query($query);
			$data = array();
			$key = 0;

			while($room = $result->fetch_assoc()) {
				$data[$key] = $room;

				$query2 = "select * from equipment where room_id='".$room['room_id']."' AND hub_serial_number='".$_GET['serialNo']."'";
				$result2 = $conn->query($query2);

				$index = 0;

				while($rows = $result2->fetch_assoc()) {
					$data[$key][$index] = $rows;
					$index++;
				}
				$key++;
			}
			//var_dump($data);
			echo json_encode($data);

    }

    if(isset($_GET['rumID'])) {
    		$selectRoom = $room->selectRoom("room_id", "'".$_GET['rumID']."'");

			if($selectRoom)
				echo "Room ID already Exists";
			else
				echo "true";
	}

}

if($_SERVER['REQUEST_METHOD'] == "POST") {

    if(isset($_POST['room_id'])) {
            	$value = array('room_name' => ($_POST['room_name']));
            	//var_dump($value);
            	$updateRoom = $room->updateRoom($value, "room_id", "'".$_POST['room_id']."'");
    }

    if(isset($_POST['rumID'])) {
    	$value = array(
    		'room_id' => $_POST['rumID'], 
    		'room_name' => $_POST['rumName'], 
    		'serial_no' => $_POST['serial_no']
		);
	
		$insert = $room->insertRoom($value);
    }
}

if ($_SERVER["REQUEST_METHOD"] == "PUT") {


	parse_str(file_get_contents("php://input"),$array);
	
	foreach ($array as $key => $value) {
		# code...
		$insertRoom = $room->insertRoom($value);
	}
	
}

	// $details = [
	// 	"room_id" => "RM001",
	// 	"room_name" => "Bed Room",
	// 	"serial_no" => "sim187",
	// 	"state" => "1"
	// ];

	// $updates = [
	// 	"room_name" => "Kitchen"
	// ];

	//insert detils
	//echo "insert detils <br/>";
	//$room->insertRoom($details);
	//update details
	//echo "update detils <br/>";
	//$room->updateRoom($updates,"room_id","'RM001'");
	//select values
	//echo "select detils <br/>";
	//var_dump($room->selectRoom("room_id","'RM001'"));
	//delete values
	//$room->deleteRoom("room_id","'RM001'");

?>