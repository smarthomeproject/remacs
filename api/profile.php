<?php

include '../utils/db-connect.php';

/**
* author :Rifhan Akram
*/
class Profile {

	private $conn;
	private $statement;
	
	function __construct(){
		# code...
		$filename = "statements.json";
        
        $handle = fopen($filename, "r");
        $content = fread($handle, filesize($filename));
        
        $this->statement = json_decode($content, true);
        
        $dbConnect = new DBConnect();
        $this->conn = $dbConnect->getInstance(); 
       
	}
        
    function __destruct(){

    }



  	//start of methods

    function getProfileStatus($column,$value,$column1,$value1){

    	$query = $this->statement["5"];

    	$query = str_replace("%TABLE%", "profile", $query);
    	$query = str_replace("%COLUMN%", $column, $query);
    	$query = str_replace("%PARAMETER%", $value, $query);
    	$query = str_replace("%COLUMN1%", $column1, $query);
    	$query = str_replace("%PARAMETER1%", $value1, $query);

    	//return $query;
    	$result=$this->conn->query($query);
    	
    	return $result->fetch_all(MYSQLI_ASSOC);

    }

    function insertProfile($valueArray){

    	$query = $this->statement["2"];

    	$temp = "";
        $index = 0;

        foreach($valueArray as $key => $value) {
            
            $index++;
            
            if($index < count($valueArray)) {
                $temp .= "" . $key . "='" . $value . "', ";
            }
            else {
                $temp .= "" . $key . "='" . $valueArray[$key] . "'";
            }

        }

       

    	$query = str_replace("%TABLE%", "profile", $query);
    	$query = str_replace("%VALUES%", $temp, $query);

    	$result=$this->conn->query($query);

    	return $result; // Error must fetch
    }    
    
    function updateProfile($valueArray,$column,$condition){
    	$query=$this->statement["3"];

    	$temp = "";
        $index = 0;

        foreach($valueArray as $key => $value) {
            
            $index++;
            
            if($index < count($valueArray)) {
                $temp .= "" . $key . "='" . $value . "', ";
            }
            else {
                $temp .= "" . $key . "='" . $valueArray[$key] . "'";
            }

        }

        $query = str_replace("%TABLE%", "profile", $query);
        $query = str_replace("%VALUES%", $temp, $query);
        $query = str_replace("%COLUMN%", $column, $query);
        $query = str_replace("%PARAMETER%", $condition, $query);

        $result = $this->conn->query($query);

        return $result;

    } 

    function deleteProfile($column,$value){
    	$query = $this->statement["4"];
        
        $query = str_replace("%TABLE%", "profile", $query);
        $query = str_replace("%COLUMN%", $column, $query);
        $query = str_replace("%VALUE%", $value, $query);
        
        $result = $this->conn->query($query);
       
        return $result;

    }

    //end of methods   

}





/* Testing  */

	$profile= new Profile();

	$newProfile=[

		"equipment_address" => "mom1020",
		"username" => "Akram",
		"profile" => "vacation",
		"state" => "1",

	];
	
	$profile->insertProfile($newProfile);

	
/*Testing ends  */

?>