var count=-1;
var roomDetails = {};
var equipmentDetails = {};
$("#new_room").click(function(e){addRoom(e)});


function addRoom(e){
  var serial = $("#serial_no").val();

  if(checkNull(count) && serial!=""){
    $("#serial_no_error").text("");
    $("#serial_no").prop("disabled",true);
    count+=1;
    if(count<=10){

      if(count==0){
        $(".confirm").append('<input type="submit" name="submit" class="btn btn-primary" id="confirm" value="confirm" >');
        $("[name=submit]").bind("click",function(){onConfirm()});
      } 
        var content='';
        // content+=('');
        content+=('<label class="text-red" name="error" id="err'+count+'"  >* </label>');
        content+=('<div class="box box-info id="box'+count+'" ">');
        content+=('<div class="box-header with-border><p class="text-light-blue">Room No :'+(count+1)+'</p><div class="box-tools pull-right"><button class="btn btn-box-tool" data-toggle="collapse" data-target="#content'+count+'" aria-expanded="false" aria-controls="content'+count+'"><i  class="fa fa-minus"></i></button><button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times" id="r'+count+'"></i></button></div></div><fieldset class="collapse" id="content'+count+'"><div class="box-body"  style="display: block;>');
        content+=('<div class"form-group"><label>Room Name</label><input type="text " name="room_no'+count+'" class="form-control" placeholder="Enter name"></div>');
        content+=('<div class="form-group"><label>Serial No:</label><div class="input-group"><div class="input-group-addon"><i class="fa fa-laptop"></i></div><input type="text" name="room_no'+count+'" class="form-control" id="serial_no"></div></div>');
        content+=('<label>Add Equipments</label> <button id="res'+count+'" class="btn btn-box-tool"><i class="glyphicon glyphicon-plus-sign"></i></button>')
        content+=('<div id="room'+count+'"> </div>');
        content+=('</div>');
        content+=('</div>');
        content+=('</fieldset>');
       
      $("#rooms").append(content);
      $('.collapse').collapse();
      $("#err"+count).hide(); 

      $("#r"+count).bind("click", function(remove){
        console.log(remove);
        var box="box"+(remove.toElement.id.substring(1));
        $("#"+box).hide();
        
      });

      $("#res"+count).bind("click",function(e){        
          var no=e.target.id.substring(3);
        var no_equip=document.getElementsByName("equipment"+no);
        if(no_equip.length<=4){
        
          $("#room"+no).append('<div class="form-group">')
          $("#room"+no).append('<input type="checkbox" data-toggle="toggle" class="toogle" id="e'+no_equip.length+'r'+count+'" data-onstyle="success" data-offstyle="danger">');
          $("#room"+no).append('<div class="col-xs-5"><input type="text" name="equipment'+no+'" class="form-control" placeholder="Enter name"></div>');
          $("#room"+no).append('</div>');

           $(function() {
          $('.toogle').bootstrapToggle({
          on: 'Bulb',
          off: 'Fan'
          });

        })
        }else{
          $("[name=error]").hide(); 
          $("#err"+no).show();
          document.getElementById("err"+no).innerHTML="Only a maximum of 5 equipments!";
          console.log(no);
          
        } 

          
        });
      
    }else{
      $("#add_room").hide();
    }
  }else{
    if(serial==""){
      document.getElementById("serial_no_error").innerHTML="Please Fill Serial Number to Continue!";

      console.log('serial err');
    }
    
  }   

}

function onConfirm(){

  var serialNo=(document.getElementById("serial_no").value);
  //check for user null
  if(checkNull(count) && CheckUser()){ 
    $("[name=error]").hide();
    for(var i=0 ; i<=count ; i++){
      //push room details to array
      console.log(count);
      var room=document.getElementsByName("room_no"+i);
      var equipments=document.getElementsByName("equipment"+i);
      console.log("equipments");
      console.log(equipments.length);
      var details={
        "room_id":serialNo+(i+1),
        "room_name":room[0].value,
        "serial_no":serialNo,
        "state":0
      }
      
      roomDetails["room"+(i+1)] = details;
      
      for(var r=1 ; r<=equipments.length ; r++){
        //push equipments of relevant rooms to array
        console.log('room'+i);
        console.log('equipment'+r);
        var type=getEquipmentType(r-1,i);
        var details={
          "hub_serial_number":serialNo,
          "equipment_number":(r),
          "room_id":serialNo+(i+1),  
          "equipment_name":type+"_"+(equipments[r-1].value),
          "state":0
        }
        equipmentDetails["equipment"+(r+i)] =  details;
        console.log(equipmentDetails);
      }
      
    }
    
    //push users details to json object
    var details = document.getElementsByName("user");
    var userDetails = {
      "username":details[1].value,
      "name":details[0].value,
      "email":details[2].value,
      "address":details[3].value,
      "password":details[4].value,
      "serial_no":details[6].value,
      "profile":"movie"
    }
    console.log(userDetails);
    insertUser(userDetails);         
  } 
  
}

function getEquipmentType(e,r){
  var type=$("#e"+e+"r"+r).prop('checked');
    
  return (type==true?"B":"F");
}

function checkNull(count){
  if(count==0)
    return true;
  else{
    for(var i=1 ; i<=count ; i++){
      var rooms=document.getElementsByName("room_no"+(i));

      for(var r=0 ; r<rooms.length ; r++){

        if(rooms[r].value==""){
          $("[name=error]").hide(); 
          $("#err"+i).show();
          document.getElementById("err"+i).innerHTML="please fill all fields!";
          console.log('room no'+i);
          console.log('field'+r);

          return false;
        }
        var equipmentCount=document.getElementsByName("equipment"+i);

        for(var e=0 ; e<equipmentCount.length ; e++){

          if(equipmentCount[e].value==""){
            $("[name=error]").hide(); 
            $("#err"+i).show();
            document.getElementById("err"+i).innerHTML="Please fill all fields!";
            console.log('room no'+i);
            console.log('field'+e);

            return false;
          }
        }
      }
      
    }
    return true;
  } 
}

function getUsername(){
  var username=$("#username").val();
  window.userExist=0;
  if(username != ""){ 
    var data={
      "username":username
    }
      $.ajax({
        url:"api/user.php",
        type:"GET",
        data:data,
        success:function(dat){
            console.log(dat.length);
            if(dat.length == 2){
              document.getElementById("user_error").innerHTML=data["username"]+" is Available";              
              window.userExist=1;
              $("#user_error").removeClass("text-red").addClass("text-green");
            }else if(dat.length >2){
              document.getElementById("user_error").innerHTML=data["username"]+" is Not Available";
              ///add error class here
              $("#user_error").removeClass("text-green").addClass("text-red");
              window.userExist=0;
              
            }
          
        },
        error:function(){
          console.log('error');
        }
      });
  }else{
    $("#user_error").text("");
    
  }   
}

function CheckUser(){
  var details = document.getElementsByName("user");
  var number=0;
  for(var i=0 ; i<details.length ; i++){
    var value=details[i];
    if( value.value == ""){
      $("#"+value.id+"_error").text("Please Fill "+value.id);
      $("#"+value.id).css("border-color","red");
      number=1;
    }else if(value.value != ""){
      $("#"+value.id+"_error").text("");
      $("#"+value.id).css("border-color","white");

    }
  }  
    
  if(number == 1)
    return false;
  else if(number == 0){
    var pass=$("#password").val()
    var confirm=$("#confirm_password").val();
    
    if((pass === confirm)){
      $("#password_error").text("");
        if(window.userExist == 1 && window.emailExist == 1 && window.serialExist == 1)
          return true;
        else if(window.userExist == 0 && window.emailExist == 0 && window.serialExist == 0)
          return false;
    }else{
      $("#password_error").text("Passwords Do Not Match!");
      return false;
    }
  }

}
    
function insertUser(userDetails){
  $.ajax({
    url:"api/user.php",
    type:"PUT",
    data:userDetails,
    success: function(e){
      console.log(e);
      insertRoom(roomDetails);
      return true;
    },
    error: function (){
      console.log("error");
      return false;
    }
  })  
}

function insertRoom(roomDetails){
  $.ajax({
    url:"api/room.php",
    type:"PUT",
    data:roomDetails,
    success: function(e){
      insertEquipment(equipmentDetails); 
      return true;
    },
    error: function(){
      
      console.log('error');
      return false;
    }
  })
}

function insertEquipment(equipmentDetails){
  $.ajax({
    content:"equipment",
    url:"api/equipment.php",
    type:"PUT",
    data:equipmentDetails,
    success:function (e){
      console.log(e);
    },
    error:function (){
      console.log('error'); 
    }
  })
}


/*check email address */
function getEmail(){
  var email=$("#email").val();
  window.emailExist=0;
  if(email != ""){ 
    var data={
      "email":email
    }
      $.ajax({
        url:"api/user.php",
        type:"GET",
        data:data,
        success:function(dat){
            console.log(dat.length);
            if(dat.length==2){
              document.getElementById("email_error").innerHTML=data["email"]+" is available";              
              window.emailExist=1;
              $( "#email_error" ).removeClass( "text-red" ).addClass( "text-green" );
            }else if(dat.length>2){
              document.getElementById("email_error").innerHTML=data["email"]+" is not available";
              $( "#email_error" ).removeClass( "text-green" ).addClass( "text-red" );
              ///add error class here
              window.emailExist=0;
              
            }
          
        },
        error:function(){
          console.log('error');
        }
      });
  }else{
    $("#email_error").text("");
    
  }   
}

function getSerial(){
  var serial=$("#serial_no").val();
  window.serialExist=0;
  if(serial != ""){ 
    var data={
      "serial":serial
    }
      $.ajax({
        url:"api/user.php",
        type:"GET",
        data:data,
        success:function(dat){
            console.log(dat.length);
            if(dat.length==2){
              document.getElementById("serial_no_error").innerHTML=data["serial"]+" is available";              
              window.serialExist=1;
              $( "#serial_no_error" ).removeClass( "text-red" ).addClass( "text-green" );
            }else if(dat.length>2){
              document.getElementById("serial_no_error").innerHTML=data["serial"]+" is not available";
              $( "#serial_no_error" ).removeClass( "text-green" ).addClass( "text-red" );
              ///add error class here
              window.serialExist=0;
              
            }
          
        },
        error:function(){
          console.log('error');
        }
      });
  }else{
    $("#serial_no_error").text("");
    
  }   
}

