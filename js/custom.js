var full="";
var content = "";
var slider ="";
var username = "murzeus";
$(document).ready(function() {
	var request = $.ajax({
	  url: "functions.php",
	  method: "POST",
	  data: { username : "'murzeus'" },
	  dataType: "json"
	});
	 
	request.done(function(data) {
		fillRooms(data);
		document.getElementById("rooms").innerHTML = full;
		$(function() {
		    $('.toggle').bootstrapToggle();

		});
		$(function () {
	        $('.slider').ionRangeSlider({
	            min : 0,
	            max : 10
	        });
	        
	    });
	});
	 
	request.fail(function(jqXHR,textStatus) {
	  console.log(textStatus);	
	});
});

function stateChange(value){
  var name = $(value).attr("name");
  var id = $(value).attr("id");
  var slider = $('[id='+ name +']').data('ionRangeSlider');
  if(!$('#'+id).is(':checked')){
  	$('[name='+ name +']').prop('disabled', true);
  	if(slider!= null)
  		slider.update({disable:true});
  }
  else{
  	$('[name='+ name +']').prop('disabled', false);
  	if(slider!= null)
  		slider.update({disable:false});
  }
  $('[id='+ id +']').prop('disabled', false);
  getValue(value,$('#'+id).is(':checked'));
}

function fillRooms(data){
	$.each(data, function(i, item) {
		content += "<div class='col-md-4'><table class='table'><thead><tr class='roomName'><th colspan = '1'>" + item.room_name+ "</th><th><input type='checkbox'  class = 'toggle' checked id = '" + item.room_id + "' data-size='mini' name = '"+ item.room_name +"' onchange = 'stateChange(this)' data-style='slow'></th></tr></thead>";		
	    $.each(item, function(key, value) {
	    	
	    	if (value.equipment_name) {
	    		//console.log(value.equipment_name);
	    		if(value.equipment_name.charAt(0) == "B"){
	    			if(value.state == 9)
	    				var check = 'checked';
	    			else
	    				var check = 'unchecked';
	    			content	= content + "<tbody><tr class='equipment'><td>" + value.equipment_name.substr(5) + "</td><td><span><input class = 'toggle chamaras-button' type='checkbox' id = '" + value.equipment_name + "' data-size='mini'" + check + " data-toggle='toggle' data-eqno = '" + value.equipment_number + "' data-roomno = '"+ item.room_id +"' name = '"+ item.room_name +"' data-style='slow' onchange ='getValue(this)'><img src='ajax-loader.gif' class='load' id='" + value.equipment_number + "' name = '"+item.room_id+"' style='display: none;'><img src='img/tick.gif' class='ack' id='" + value.equipment_number + "' name = '"+item.room_id+"' style='display: none;'><img src='img/error.png' class='error' id='" + value.equipment_number + "' name = '"+item.room_id+"' style='display: none;'></span></td></tr></tbody>";
	    			
	    		}
	    		else if(value.equipment_name.charAt(0) == "F"){
	    			content	= content + "<tbody><tr class='equipment'><td>" + value.equipment_name.substr(4) + "</td><td><div class='col-sm-8'><input class = 'slider' id='"+ item.room_name +"' name = '" + value.equipment_name + "' type='text' name='range_1' data-roomno = '"+ item.room_id +"' data-eqno = '" + value.equipment_number + "' value='" + value.state+ "' onchange ='getValue(this)'/></div><img src='ajax-loader.gif' class='load' id='" + value.equipment_number + "' name ='"+item.room_id+"' style='display: none;'><img src='img/tick.gif' class='ack' id='" + value.equipment_number + "' name = '"+item.room_id+"' style='display: none;'><img src='img/error.png' class='error' id='" + value.equipment_number + "' name = '"+item.room_id+"' style='display: none;'></td></tr></tbody>";
	    		}
    		}
		});	
	    content =  content + "</table></div>";
		full = full + content;
		content = "";
	});
}

// Testing .....

function getValue(data,main){
	//console.log(data);
	if(main != null){
		if( main ) {
			console.log("Main Switch : " + data.id + " State : " + 1 );
			sendRoom(data.id,1);
		}
		else {
			console.log("Main Switch : " + data.id + " State : " + 0 );
			sendRoom(data.id,0);
		}
	}
	else if(data.name.charAt(0) == 'F'){
		var roomno = $("#" + data.id).data("roomno");
		var eqno = $("#" + data.id).data("eqno");
		console.log("Room : "+ $("#" + data.id).data("roomno") +" Fan Id : " + eqno + " State : " + data.value );
		sendData(eqno,data.value,roomno);
	}
	else if(data.id.charAt(0) == 'B'){
		//console.log("Room : "+ $("#" + data.id).data("roomno") +" Bulb Id : " + $("#" + data.id).data("eqno") + " State : " + $('#'+data.id).is(':checked') );
		var roomno = $("#" + data.id).data("roomno");
		var eqno = $("#" + data.id).data("eqno");
		if($('#'+data.id).is(':checked')){
		 	sendData(eqno,9,roomno);	
		}
		else{
			sendData(eqno,0,roomno);
		}
		
	}
}

function sendData(name,value,room){
	var request = $.ajax({
	  url: "http://192.168.1.3:3000/update",
	  method: "POST",
	  data: JSON.stringify({
	  	"username" : username,
	  	"id" : name, 
	  	"state" : value , 
	  	"room_name" : room
	  }),
	  // dataType: "json",
	  crossDomain: true,
	  contentType: "application/json"
	});
	$('#'+name+'.ack').hide();
	$('#'+name+'.error').hide();
	$('#'+name+'.load').show(); 
	request.done(function(data) {
		$('#'+name+'.load').hide();
		$('#'+name+'.ack').show();
		console.log(data);
	});
	 
	request.fail(function(jqXHR,textStatus) {
		$('#'+name+'.load').hide();
		$('#'+name+'.error').show();
	  	console.log(textStatus);	
	});
}

function sendRoom(room,state){
	var request = $.ajax({
	  url: "http://192.168.1.3:3000/update-room",
	  method: "POST",
	  data: JSON.stringify({
	  	"username" : username,
	  	"room_name" : room, 
	  	"state" : state
	  }),
	  // dataType: "json",
	  crossDomain: true,
	  contentType: "application/json"
	});
	 
	request.done(function(data) {
		console.log(data);
	});
	 
	request.fail(function(jqXHR,textStatus) {
	  console.log(textStatus);	
	});
}


