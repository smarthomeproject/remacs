$(document).ready(function() {

	$('#editUser').hide();
	$('#editRoom').hide();
	$('#addMoreRoom').hide();

	$.ajax({
		url: "api/user.php",
		method: "GET",
		data:{ready : "ready"},
		dataType: "json",
		success: function(data) {
			getContent(data);
		}
	});

	$('#search').keyup(function() {
		var searchData= {
			search : $('#search').val()
		}

		$.ajax({
			url: "api/user.php",
			method: "GET",
			data:  searchData,
			dataType: "json",
			success: function(data) {
				//console.log(data);
				getContent(data);
			},
			error: function() {
				console.log("fail");
			}
		});

	});
});

function getContent(data) {
	
	var content = "";

	content = "<tr><th>Username</th><th>Name</th><th>Serial-No</th><th></th></tr>";
	
	$.each(data, function(key, value) {
		content += "<tr><td>"+value.username+"</td><td>"+value.name+"</td><td>"+value.serial_no+"</td><td><button id='"+value.username+"' name='edit' class='btn btn-box-tool' onclick='editUser(this);' ><i class='fa fa-edit'></i></button></td></tr>";
	});
	
	document.getElementById('table').innerHTML = content;
}

var roomCount = 0;
var addRoomCount = 0;
var equipmentCount = 0;

function editUser(value) {

	$('.form-control').css({"background-color":"white"});
	$('#username').css({"background-color":"#EEEEEE"});
	$('#submit').css({"background-color":"#3C8DBC"});
	$('.error').text("");


	var username = $(value).attr("id");
	var content = "";

	$.ajax({
		url: "api/user.php",
		method: "GET",
		data: {username : username},
		dataType : "json",
		success: function(data) {
			var addRoomId = "";
			$.each(data, function(key, value) {
				$('#name').val(value.name);
				$('#username').val(value.username);
				$('#email').val(value.email);
				$('#address').val(value.address);
				$('#password').val(value.password);
				$('#serial_no').val(value.serial_no);

				$.ajax({
					url: "api/room.php",
					method: "GET",
					data : {serialNo : $('#serial_no').val()},
					dataType: "json",
					success: function(datas) {
						$.each(datas, function(key, value) {
							addRoomId = value.room_id+"_add_room";
							var setId = "";
							var divID = value.room_id+"div";
							var err = value.room_name+"_err";
							var button = value.room_id+"button";
							content += "<div class='col-md-6' id="+divID+"><div class='box box-info'><div class='box-tools pull-right'><button class='btn btn-box-tool' data-toggle='collapse' data-target=#"+button+" aria-expanded='false' aria-controls='content'><i class='fa fa-minus'></i></button></div><fieldset class='collapse in' id="+button+"><div class='box-body'><div class='form-group'><label>Room Name</label><span id="+err+" name='Room Name' style='float:right; color:#DD4B39;'></span><input type='text' class='form-control' id="+value.room_name+" value="+value.room_name+" placeholder='Enter Room Name'><label>Room ID</label><input type='text' class='form-control' id="+value.room_id+" value="+value.room_id+" disabled></div><table class='table table-condensed'><tbody><th></th><th>Equipment Name</th><th>Equipment ID</th>";
							equipmentCount = 0;
							$.each(value, function(key, value) {
								if(value.equipment_number){
									equipmentCount++;

									setId += value.equipment_number+"_";

									var equipType = "<img src='img/fan.gif'>";
									if((value.equipment_name).charAt(0) == "B"){
										equipType = "<img src='img/bulb.gif'>";
									}

									content += "<tr><td>"+equipType+"</td><td><input type='text' class='equipName' id="+value.equipment_number+" value="+(value.equipment_name).substr(1)+" placeholder'Enter Equipment Name'></td><td><input type='text' class'form-control' id="+value.equipment_number+" value="+value.equipment_number+" disabled style='background-color:#EEEEEE;'></td></tr>";
								}
							});
							
						
							content += "</tbody></table><br><button id="+setId+" class='btn btn-block btn-primary' onclick='roomOnSubmit(this,"+value.room_id+","+value.room_name+");'>Submit</button></div></div></fieldset></div><div id="+addRoomId+"></div>"
							roomCount++;
							addRoomCount++;
						});
					document.getElementById('addRoom').innerHTML = content;
					if(addRoomCount < 10) {
						$('#'+addRoomId).append("<button id="+username+" onclick='addRoom(this);'>Add Room</button>");
					}
					},
					error: function() {
						console.log("fail");
					}
				});
			});
			
			$('#editUser').fadeIn();
			$('#editRoom').fadeIn();
		},
		error: function() {
			console.log("fail");
		}
	});
}

function addRoom(value) {
	
	if(addRoomCount < 10) {

			var id = $(value).attr("id");
			$('#addMoreRoom').show();
			//console.log(id);
			
			$('#addUserName').val(id);
	}

}

function onSubmit() {
	
	if (($('#name').val()) && ($('#email').val()) && ($('#address').val()) && ($('#password').val()) && ($('#serial_no').val())) {

		$('#submit').css({"background-color":"#3C8DBC"});

		var data = {
			username : $('#username').val(),
			name : $('#name').val(),
			email : $('#email').val(),
			address : $('#address').val(),
			password : $('#password').val(),
			serial_no : $('#serial_no').val()
		};
		
		$.post('api/user.php', data, function(data) {
			$('#editUser').fadeOut();
			$('#jj').css({"float":"right"});
			$.get('api/user.php', {ready : "ready"}, function(data) {
					getContent(data);
			}, 'json');
		});
	}
	else {
		
		$('#submit').css({"background-color":"#DD4B39"});
		$('.error').text("");

		var val = [ $('#name').val(), $('#email').val(), $('#address').val(), $('#password').val(), $('#serial_no').val() ];
		var err = [ "name_err", "email_err", "address_err", "password_err", "serial_no_err" ];

		isEmpty(val, err);
	}
}

function isEmpty(val, err) {
	for(var i=0; i<val.length; i++) {
		if(!val[i]) {
			var name = $('#'+err[i]).attr('name');
			$('#'+err[i]).text("Please type your "+name+"!");
		}
	}
}

function roomOnSubmit(val, roomid, name) {
	var roomID = $(roomid).val();
	var roomName = ($(name).attr("id")); 
	var buttonId = $(val).attr("id");

	$('#'+buttonId).css({"background-color":"#3C8DBC"});
	$('#'+roomName+'_err').text("");
	$('.equipName').css({"border":""});

	var idS = $(val).attr("id").split("_");
	var count = 0;

	if(!($('#'+roomName).val())) {
		$('#'+roomName+'_err').text("Please type a valid Room Name!");
		$('#'+buttonId).css({"background-color":"#DD4B39"});
	}
	else{
		count++;
	}

	for(var i=0; i<(idS.length-1); i++) {

		if(!($('#'+idS[i]).val())) {
			$('#'+idS[i]).css({"border":"1px solid red"});
			$('#'+buttonId).css({"background-color":"#DD4B39"});
		}
		else {
			count++;
		}
	}

	var eqName = [];
	var eqAdd = [];

	if(count == idS.length){
		for(var i=0; i<idS.length-1; i++) {
			eqName.push($('#'+idS[i]).val());
			eqAdd.push(idS[i]);
		}
		//console.log(arr);
		var display = {
			"room_id" : roomID,
			"room_name" : $('#'+roomName).val(),
			"equip" : eqName,
			"addr" : eqAdd
		};

		$.post('api/room.php', display, function(data) {
			$.post('api/equipment.php', display, function(data) {

				$('#'+roomID+'div').fadeOut();
				roomCount--;

				if(roomCount == 0){
					$('#editRoom').fadeOut();
				}
			});
		});
	}
}

function onSubmitAddMoreRoom() {
	
	var rumId = $('#room_id').val();
	var rumName = $('#room_name').val();
	var eq_no1 = $('#eq_no1').val();
	var eq_no2 = $('#eq_no2').val();
	var eq_no3 = $('#eq_no3').val();
	var eq_no4 = $('#eq_no4').val();
	var eq_no5 = $('#eq_no5').val();
	var eq_name1 = $('#eq_name1').val();
	var eq_name2 = $('#eq_name2').val();
	var eq_name3 = $('#eq_name3').val();
	var eq_name4 = $('#eq_name4').val();
	var eq_name5 = $('#eq_name5').val();

	var eqNu = [eq_no1, eq_no2, eq_no3, eq_no4, eq_no5];
	var eqName = [eq_name1, eq_name2, eq_name3, eq_name4, eq_name5 ];

	var flag = 0;

	for(var i=0; i<5; i++) {
		if(eqNu[i] && eqName[i])
			flag++;
	}

	if (!rumId && !rumName && (flag==0)) {
		$('#room_id_err').text("Please type Room ID");
		$('#room_name_err').text("Please type Room Name");
		$('#eq_err').text("Atleast one equipment details should be filled, Please check Equipment number and name!");
	}
	else if(!rumId && !rumName && (flag>=1)) {
		$('#room_name_err').text("Please type Room Name");
		$('#room_id_err').text("Please type Room ID");
		$('#eq_err').text("");
	}
	else if(rumName && !rumId && (flag==0)) {
		$('#room_name_err').text("");
		$('#room_id_err').text("Please type Room ID");
		$('#eq_err').text("Atleast one equipment details should be filled, Please check Equipment number and name!");
	}
	else if(rumId && !rumName && (flag==0)) {
		$('#room_id_err').text("");
		$('#room_name_err').text("Please type Room Name");
		$('#eq_err').text("Atleast one equipment details should be filled, Please check Equipment number and name!");
	}
	else if(!rumId && rumName && (flag>=1)) {
		$('#room_id_err').text("Please type Room ID");
		$('#room_name_err').text("");
		$('#eq_err').text("");
	}
	else if(!rumName && rumId && (flag>=1)) {
		$('#room_id_err').text("");
		$('#room_name_err').text("Please type Room Name");
		$('#eq_err').text("");
	}
	else if(rumId && rumName && (flag==0)) {
		$('#room_id_err').text("");
		$('#room_name_err').text("");
		$('#eq_err').text("Atleast one equipment details should be filled, Please check Equipment number and name!");
	}
	else if(rumId && rumName && (flag>=1)){
		$('#room_id_err').text("");
		$('#room_name_err').text("");
		$('#eq_err').text("");

		var eqNu = [eq_no1, eq_no2, eq_no3, eq_no4, eq_no5];
		var eqName = [eq_name1, eq_name2, eq_name3, eq_name4, eq_name5 ];

		var nEqNu = [];
		var nEqName = [];

		for(var i=0; i<5; i++) {
			if(eqNu[i] && eqName[i]) {
				nEqNu.push(eqNu[i]);
				nEqName.push(eqName[i]);
			}
		}

		var dataRumId  = {
			"rumID" : rumId,
			"rumName" : rumName,
			"eq_no" : nEqNu,
			"eq_name" : nEqName,
			"serial_no" :  $('#serial_no').val()
		};
				
		$.get('api/room.php', dataRumId, function(data) {
			//console.log(data);
			if(data == "true") {
				$.get('api/equipment.php', dataRumId, function(data) {
					//console.log(data);
					if(data == "true") {
						$.post("api/room.php", dataRumId, function(data) {
							//console.log("room added");
							$.post("api/equipment.php", dataRumId, function(data) {
								//console.log("eq added");
								$('#addMoreRoom').fadeOut();
								$.get('api/user.php', {ready : "ready"}, function(data) {
											getContent(data);
								}, 'json');
							});
						});
					}
					else {
						$('#eq_err').text(data);
					}
				});
			}
			else {
				$('#room_id_err').text(data);
			}
		});

	}

	
	addRoomCount++;
}




