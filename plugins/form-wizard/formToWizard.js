/* Created by jankoatwarpspeed.com */


(function($) {


  $.fn.formToWizard = function(options) {
      options = $.extend({  
          submitButton: "" 
      }, options); 
      
      var element = this;
      window.userDetails=[];

      var steps = $(element).find("fieldset");
      var count = steps.size();
      var submmitButtonName = "#" + options.submitButton;
      $(submmitButtonName).hide();

      // 2
      $(element).before("<ul id='steps'></ul>");

      steps.each(function(i) {

          $(this).wrap("<div id='step" + i + "'></div>");
          $(this).append("<p id='step" + i + "commands'></p>");

          // 2
          var name = $(this).find("legend").html();
          $("#steps").append("<li id='stepDesc" + i + "'>Step " + (i + 1) + "<span>" + name + "</span></li>");
          console.log(i);
          if (i == 0) {
              
              createNextButton(i);
              selectStep(i);
          }
          else if (i == count - 1) {
              $("#step" + i).hide();
              createPrevButton(i);
          }
          else {
              $("#step" + i).hide();
              createPrevButton(i);
              createNextButton(i);
          }
      });

      function createPrevButton(i) {
          var stepName = "step" + i;
          $("#" + stepName + "commands").append("<a href='#' id='" + stepName + "Prev' class='prev'>< Back</a>");

          $("#" + stepName + "Prev").bind("click", function(e) {
              $("#" + stepName).hide();
              console.log(stepName);
              $("#step" + (i - 1)).show();
              $(submmitButtonName).hide();
              selectStep(i - 1);
          });
      }

      function createNextButton(i) {
          var count=i;
          var stepName = "step" + i;

          $("#" + stepName + "commands").append("<a href='#' id='" + stepName + "Next' class='next'>Next ></a>");

          $("#" + stepName + "Next").bind("click", function(e) {
              currentStep="step"+i;

              if (i==0) {

                  var userDetails=document.getElementsByName('add_user');
                  //pass to is empty
                  var result="success";

                  if (result=="success") {
                      $("#" + stepName).hide();
                      $("#step" + (i + 1)).show();
                      if (i + 2 == count)
                          $(submmitButtonName).show();
                      selectStep(i + 1);
                  };

              }else if(i==1){
                
                var roomDetails=document.getElementsByName('add_room');
                //pass to is empty
               
                var result="success";

                if (result=="success") {

                  setRoomDetails(count);
                  
                  count+=1;

                  if (count<=10) {

                    $("#" + stepName).hide();
                    $("#step" + (i)).show();

                    

                    // if (i + 2 == count)
                    $(submmitButtonName).show();
                    selectStep(i);

                  }else if(count==11){

                    

                    $("#" + stepName).hide();
                    $("#step" + (i+1)).show();
                    // if (i + 2 == count)
                    $(submmitButtonName).show();
                    selectStep(i+1);



                  }
                  //submit button click event moves to summary
                  $(submmitButtonName).click(function(){

                    $("#" + stepName).hide();
                    $("#step" + (i+1)).show();
                    // if (i + 2 == count)
                    $(submmitButtonName).show();
                    getSummary();
                    selectStep(i+1);
                    
                    document.getElementById('submit').innerHTML="Confirm";

                  })
                    
                };
              }
                  
          });
      }


                    
      
      
      function selectStep(i) {
          
          $("#steps li").removeClass("current");
          $("#stepDesc" + i).addClass("current");
          
      }

      //summary
      function getSummary () {
        
        var count=userDetails.length;
        var content='<table class="table table-striped">  <tbody>';

        for(var i=0 ; i<count ; i++){
          var object=userDetails[i];
          if(i==0){
            content+='<th>"'+userDetails[0].name+'"</th>'
          }
          for(var key in object) {
              console.log(object[key]);
          }
           
      }

       content+='</tbody></table>';
       document.getElementById('summary').innerHTML=content;
      }
      
      function setRoomDetails (step) {
        var room="room "+step;
        var result='successfull'; 
        var non=[];
          
          if (step==1) {
            

              var temp={
                "name":document.getElementById('name').value,
                "username":document.getElementById('username').value,
                "email":document.getElementById('email').value,
                "address":document.getElementById('address').value,
                "password":document.getElementById('password').value,
                "serial_no":document.getElementById('serial_no').value
              }
              userDetails.push(temp);
              console.log(temp);
              
          }

          if (step<=10) {
            var serial=document.getElementById('serial_no').value;
            var roomId=serial.substring(0,3)+step;
            var rooms={

              
                    "room_no":step,
                    "room_id":roomId,
                    "room_name":document.getElementById('room_name').value,
                    "room_serial":document.getElementById('room_serial_no').value,
                    "equipment_1":document.getElementById('equipment_1').value,
                    "equipment_2":document.getElementById('equipment_2').value,
                    "equipment_3":document.getElementById('equipment_3').value,
                    "equipment_4":document.getElementById('equipment_4').value,
                    "equipment_5":document.getElementById('equipment_5').value,
                        
            } 


            userDetails.push(rooms);

          console.log(userDetails);
            
            
      }
      //end of method

      

    }
      
  }
})(jQuery); 
          
          

            
          

          
        






/* methods to complete

  getEquipmentId();
  getRoomId();
  checkisEmpty();
  checkequalpassword();
  must find a method to empty text box after next button

  concat B for bulb and F for fan
 */
