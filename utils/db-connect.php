<?php

class DBConnect extends mysqli{
    
    protected static $instance;
    
    //constructor
    function __construct() {
        
        //file path
        $filename = "../utils/config.json";
        //read config file
        $handle = fopen($filename, "r");
        $content = fread($handle, filesize($filename));
        
        //parse json
        $configs = json_decode($content);
        
        //pass parameters to parent constructor
        parent::__construct($configs->database->host,
                            $configs->database->username,
                            $configs->database->password,
                            $configs->database->name);
        
        if( mysqli_connect_errno() ) {
            throw new exception(mysqli_connect_error(), mysqli_connect_errno()); 
        }
    }
    
    //desctuctor
    function __destruct() {
        
    }
    
    public function getInstance() {
        if( !self::$instance ) {
            self::$instance = new self(); 
        }
        return self::$instance;
    }
    
    public function query($query) {
        //sanitise the sql query
        // $query = $this->escape_string($query);
        // var_dump($this);
        //return results object
        if(!$this->real_query($query)) {
            throw new exception( $this->error, $this->errno );
        }
        else {
            return new mysqli_result($this);    
        }
        
    }
}
?>