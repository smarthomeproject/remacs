<!DOCTYPE html>
<html>

    
    <head>
        <meta charset="UTF-8">
        <title>ReMacs | Dashboard</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
        name="viewport">
        <!-- Bootstrap 3.3.4 -->
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <!-- Font Awesome Icons -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"
        rel="stylesheet" type="text/css">
        <!-- Ionicons -->
        <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"
        rel="stylesheet" type="text/css">
        <!-- Theme style -->
        <link href="css/AdminLTE.min.css" rel="stylesheet" type="text/css">
        <!-- AdminLTE Skins. We have chosen the skin-blue for this starter page.
        However, you can choose any other skin. Make sure you apply the skin class
        to the body tag so the changes take effect. -->
        <link href="css/skins/skin-blue.min.css" rel="stylesheet" type="text/css">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media
        queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file://
        -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet">

        <link href="css/skins/skin-blue.min.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="css/usage.css">
        
        
    </head>
    
    <body class="skin-blue sidebar-mini sidebar-collapse" >
        <div class="wrapper">
            <!-- Main Header -->
            <header class="main-header">
                <!-- Logo -->
                <a href="index2.html" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>R</b>MC</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Re</b>Macs</span>
        </a>
                <!-- Header Navbar -->
                <nav class="navbar navbar-static-top" role="navigation">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
                    <!-- Navbar Right Menu -->
                    <div class="navbar-custom-menu">
                        <div class="btn-group btn-group-sm" style="margin-left:110px;" data-toggle="buttons">
                            
                        </div>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel (optional) -->
                    <div class="user-panel">
                        

                    </div>
                    <!-- search form (Optional) -->
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>
                    <!-- /.search form -->
                    <!-- Sidebar Menu -->
                    <ul class="sidebar-menu">
                        <li class="header">HEADER</li>
                        <!-- Optionally, you can add icons to the links -->
                        <li class="active">
                            <a href="starter.html"><i class="fa fa-sliders"></i> <span>Add User</span></a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-calendar"></i> <span>Edit User</span></a>
                        </li>
                        
                    </ul>
                    <!-- /.sidebar-menu -->
                </section>
                <!-- /.sidebar -->
            </aside>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>Add Users</h1>
                    <ol class="breadcrumb">
                        <li>
                            <a href="#"><i class="fa fa-dashboard"></i> Admin</a>
                        </li>
                        <li class="active">Add User</li>
                    </ol>
                </section>
                <!-- Main content -->
                <section class="content">
                    <div class="container">
                      <div class="col-md-6">
                          <div class="box box-info">
                            <div class="box-header with-border">
                              <h3 class="box-title">User Details</h3>
                              <div class="box-tools pull-right">
                                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                
                              </div>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                              <div class="form-group">
                                <label>Name</label><span class="text-red" id="name_error" style="float:right;"></span>
                                <input type="text" class="form-control" id="name" name="user"  placeholder="Enter name">
                              </div>

                              <div class="form-group" >
                                
                                <label for="exampleInputEmail1">Username</label><span class="text-red" id="user_error" style="float:right;"></span>
                                <input type="email" oninput="getUsername()" name="user"  class="form-control" id="username" placeholder="Enter username">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputEmail1">Email </label><span class="text-red" id="email_error" style="float:right;"></span>
                                <input type="email" class="form-control" oninput="getEmail()" name="user" id="email" placeholder="Enter email">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputEmail1">address</label><span class="text-red" id="address_error" style="float:right;"></span>
                                <input type="email" class="form-control" name="user"  id="address" placeholder="Enter address">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputEmail1">Password</label><span class="text-red" id="password_error" style="float:right;"></span>
                                <input type="password" class="form-control" name="user"  id="password" placeholder="Enter password">
                              </div>
                              <div class="form-group">
                                <label for="exampleInputEmail1">Confirm Password</label><span class="text-red" id="confirm_password_error" style="float:right;"></span>
                                <input type="password" class="form-control" name="user"  id="confirm_password" placeholder="Confirm">
                              </div>
                              <div class="form-group">
                                <label>Serial No:</label><span class="text-red" id="serial_no_error" style="float:right;"></span>
                                <div class="input-group">
                                  <div class="input-group-addon">
                                    <i class="fa fa-laptop"></i>
                                  </div>
                                  <input type="text" name="user" oninput="getSerial()" class="form-control" id="serial_no">
                                </div><!-- /.input group -->
                              </div>

                            </div><!-- /.box-body -->
                            
                          </div>
                          <div class="confirm">
                            
                          </div>
                        <!--add room and add_room divs here -->
                      
                        <!-- <div class="box box-info">
                            <div class="box-header with-border">
                              <h3 class="box-title">User Details</h3>
                              <div class="box-tools pull-right">
                                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                
                              </div>
                            </div>
                            <div class="box-body">
                              
                            </div>
                        </div> -->
                      
                        <!--add room button goes here-->
                          
                        
                      </div>
                      
                      <div class="col-md-5">
                        <div id="rooms">  
                        </div>
                          <div class="box box-danger" id="add_room">
                            <div class="box-header with-border">
                              <h3 class="box-title">Add Room</h3>
                              <div class="box-tools pull-right">
                                <button class="btn btn-box-tool" id="new_room" ><i class="fa fa-user-plus"></i></button>
                                
                            </div>
                          </div><!-- /.box-header -->
                           
                        </div>
                        
                      </div>
                    </div>
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <!-- Main Footer -->
            <footer class="main-footer">
                <!-- To the right -->
                <div class="pull-right hidden-xs"></div>
                <!-- Default to the left -->
                <strong>Copyright © 2015
                    <a href="#">ReMacs</a>.</strong>All rights reserved.</footer>
            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
                <!-- Create the tabs -->
                <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
                    <li class="active">
                        <a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a>
                    </li>
                    <li>
                        <a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <!-- Home tab content -->
                    <div class="tab-pane active" id="control-sidebar-home-tab">
                        <h3 class="control-sidebar-heading">Recent Activity</h3>
                        <ul class="control-sidebar-menu">
                            <li>
                                <a href="javascript::;">
                  <i class="menu-icon fa fa-birthday-cake bg-red"></i>
                  <div class="menu-info">
                    <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>
                    <p>Will be 23 on April 24th</p>
                  </div>
                </a>
                            </li>
                        </ul>
                        <!-- /.control-sidebar-menu -->
                        <h3 class="control-sidebar-heading">Tasks Progress</h3>
                        <ul class="control-sidebar-menu">
                            <li>
                                <a href="javascript::;">               
                  <h4 class="control-sidebar-subheading">
                    Custom Template Design
                    <span class="label label-danger pull-right">70%</span>
                  </h4>
                  <div class="progress progress-xxs">
                    <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
                  </div>                                    
                </a>
                            </li>
                        </ul>
                        <!-- /.control-sidebar-menu -->
                    </div>
                    <!-- /.tab-pane -->
                    <!-- Stats tab content -->
                    <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
                    <!-- /.tab-pane -->
                    <!-- Settings tab content -->
                    <div class="tab-pane" id="control-sidebar-settings-tab">
                        <form method="post">
                            <h3 class="control-sidebar-heading">General Settings</h3>
                            <div class="form-group">
                                <label class="control-sidebar-subheading">Report panel usage
                                    <input type="checkbox" class="pull-right" checked="">
                                </label>
                                <p>Some information about this general settings option</p>
                            </div>
                            <!-- /.form-group -->
                        </form>
                    </div>
                    <!-- /.tab-pane -->
                </div>
            </aside>
            <!-- /.control-sidebar -->
            <!-- Add the sidebar's background. This div must be placed immediately
            after the control sidebar -->
            <div class="control-sidebar-bg"></div>
        </div>
        <!-- ./wrapper -->
        <!-- REQUIRED JS SCRIPTS -->
        <!-- jQuery 2.1.4 -->
        <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
        <!-- Bootstrap 3.3.2 JS -->
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="js/app.min.js" type="text/javascript"></script>
        <!-- Optionally, you can add Slimscroll and FastClick plugins.
        Both of these plugins are recommended to enhance the user experience. Slimscroll
        is required when using the fixed layout. -->
        
        <script type="text/javascript" src="https://www.google.com/jsapi">
            
        </script>
        <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js">
            
        </script>
    
        <script type="text/javascript" src="js/admin.js" ></script>
    </body>
    
</html>